let burger = document.querySelector('.burger');

burger.addEventListener('click', function() {
  this.classList.toggle('is-active');
  document.querySelector('.nav').classList.toggle('visible');
})
